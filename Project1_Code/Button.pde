class Button {
	protected String name;
	protected int xpos, w, h;
    protected int ypos;
	protected color c1,c2;
    protected int tSize;

	Button() {
		this.name = "";
		this.w = 800;
		this.h = 100;
		this.xpos = width/2;
        this.tSize = 50;
	}
	
	boolean isOver()
	{
		if (mouseX < xpos-w/2 || mouseX > xpos+w/2) return false;
		if (mouseY < ypos-h/2 || mouseY > ypos+h/2) return false;
		return true;
	}

	boolean buttonClick() {
		if(!isOver()) return false;
		return true;
	}

	void draw() {
		rectMode(CENTER);
		if(!isOver()) {
            fill(c1);
        } else {
            fill(c2);
        }
		rect(xpos, ypos, w, h,10);
		fill(0);
		textAlign(CENTER,CENTER);
		textSize(tSize);
		text(name, xpos, ypos);
	}
}

class upgradeHPButton extends Button {
    upgradeHPButton() {
        this.xpos = width/2 - 165;
        this.ypos = 490;
        this.w = 300;
        this.h = 80;
        this.c1 = #FFD400;
        this.c2 = #EFC410;
        this.name = "upgrade HP : 10G";
        this.tSize = 25;
    }
}

class upgradePowerButton extends Button {
    upgradePowerButton() {
        this.xpos = width/2 + 165;
        this.ypos = 490;
        this.w = 300;
        this.h = 80;
        this.c1 = #FFD400;
        this.c2 = #EFC410;
        this.name = "upgrade Power : 30G";
        this.tSize = 25;
    }
}

class mapButton extends Button {
    JSONObject map;
    int index;

    mapButton(int i, JSONObject map) {
        this.map = map;
        this.index = i+1;
        this.c1 = #4848C1;
        this.c2 = #3838AF;
        this.tSize = 25;
        if(index < 10) {
            this.name = "Map 0" + Integer.toString(index);
        } else {
            this.name = "Map " + Integer.toString(index);
        }
        this.w = 150;
        this.h = 200;
        if(index <= 5) {
            this.xpos = 350 + (index - 1)*200;
            this.ypos = 275;
        } else {
            this.xpos = 350 + (index - 6)*200;
            this.ypos = 525;
        }
    }

    JSONObject getMap() {
        return map;
    }

    void draw() {
		rectMode(CENTER);
		if(!isOver()) {
            fill(c1);
        } else {
            fill(c2);
        }
		rect(xpos, ypos, w, h,10);
		
        fill(#3DB7CC);
        rectMode(CORNER);
        rect(xpos - 50, ypos - 30, 100, 50);

        fill(#872600);
        JSONArray values = this.map.getJSONArray("blocks");
        if(values != null) {
            for(int i = 0; i<values.size(); i++) {
                JSONObject tmp = values.getJSONObject(i);
                noStroke();
                rect(xpos - 50 + 5*tmp.getInt("x"), ypos - 30 + 5*tmp.getInt("y"), 5, 5);
            }
        }
        
        fill(0);
		textAlign(CENTER,CENTER);
		textSize(tSize);
		text(name, xpos, ypos + 50);
	}

}

class resumeButton extends Button{
    resumeButton() {
        this.xpos = width/2;
        this.ypos = 490;
        this.w = 630;
        this.h = 80;
        this.c1 = #F4EB42;
        this.c2 = #D3C731;
        this.name = "Resume";
        this.tSize = 25;
    }
}

class restartButton extends Button {
    restartButton() {
        this.xpos = width/2 - 165;
        this.ypos = 600;
        this.w = 300;
        this.h = 80;
        this.c1 = #2FE02F;
        this.c2 = #26C426;
        this.name = "Restart";
        this.tSize = 25;
    }
}

class nextLevelButton extends Button {
    nextLevelButton() {
        this.xpos = width/2 - 165;
        this.ypos = 600;
        this.w = 300;
        this.h = 80;
        this.c1 = #2FE02F;
        this.c2 = #26C426;
        this.name = "Next Level";
        this.tSize = 25;
    }
}

class goHomeButton extends Button {
    goHomeButton() {
        this.xpos = width/2 + 165;
        this.ypos = 600;
        this.w = 300;
        this.h = 80;
        this.c1 = #CE2E2E;
        this.c2 = #BC2222;
        this.name = "Go Home";
        this.tSize = 25;
    }

    goHomeButton(String isboss) {
        this.xpos = width / 2;
        this.ypos = 600;
        this.w = 630;
        this.h = 80;
        this.c1 = #CE2E2E;
        this.c2 = #BC2222;
        this.name = "Go Home";
        this.tSize = 25;
    }
}

class newButton extends Button {
    newButton() {
        super();
        this.ypos = height*2/6;
        this.c1 = #F4EB42;
        this.c2 = #D3C731;
        this.name = "New Game";
    }
}

class setButton extends Button {
    int setLevel;
    int maxLevel;
    setButton(int id) {
        super();
        this.ypos = height*3/6;
        this.c1 = #EEBC3C;
        this.c2 = #D19E2C;
        this.name = "Load Game : Lv. 1";
        this.setLevel = 1;
        this.maxLevel = 1;

        File f = new File(sketchPath("data/players.json"));
		if(!f.exists()) return;

        JSONObject players = loadJSONObject("data/players.json");

        JSONArray list = players.getJSONArray("players");
        if(list == null) {
            return;
        }

        for(int i = 0; i < list.size(); i++) {
			JSONObject player = list.getJSONObject(i);
            if(player.getInt("id") == id) {
                this.maxLevel = player.getInt("level") + 1;
                if(this.maxLevel > 15) this.maxLevel = 15;
                return;
            }
		}
        return;
    }

    boolean canAdd() {
        if((mouseX > xpos + w/2 + 15) && (mouseY - ypos) / (h/2 - 7) > (mouseX - xpos - w/2 - 10 - h * 1.7/2) / (h * 1.7 / 2 - 5) && (mouseY - ypos) / (7 - h/2) > (mouseX - xpos - w/2 - 10 - h * 1.7/2) / (h * 1.7 / 2 - 5)) return true;
        return false;
    }

    boolean canSub() {
        if((mouseX < xpos - w/2 - 15) && (mouseY - ypos) / (h/2 - 7) < (mouseX - xpos + w/2 + 10 + h * 1.7/2) / (h * 1.7 / 2 - 5) && (mouseY - ypos) / (7 - h/2) < (mouseX - xpos + w/2 + 10 + h * 1.7/2) / (h * 1.7 / 2 - 5)) return true;
        return false;
    }

    boolean buttonClick() {
        if(canAdd()) {
            if(setLevel < this.maxLevel) {
                setLevel++;
                this.name = "Load Game : Lv. " + Integer.toString(setLevel);
            }   
        }
        if(canSub()) {
            if(setLevel > 1) {
                setLevel--;
                this.name = "Load Game : Lv. " + Integer.toString(setLevel);
            }
        }
		if(!isOver()) return false;
		return true;
	}


    int getLevel() {
        return setLevel;
    }

    void draw() {
		rectMode(CENTER);
		if(!isOver()) {
            fill(c1);
        } else {
            fill(c2);
        }
		rect(xpos, ypos, w, h,10);
		fill(0);
		textAlign(CENTER,CENTER);
		textSize(50);
		text(name, xpos, ypos);
        if(canAdd()) {
            fill(#EFEF7F);
        } else {
            fill(#FFFF8F);
        }
        triangle(xpos + w/2 + 15, ypos + h/2 - 7, xpos + w/2 + 15, ypos - h/2 + 7, xpos + w/2 + 10 + h * 1.7/2, ypos);
        if(canSub()) {
            fill(#EFEF7F);
        } else {
            fill(#FFFF8F);
        }
        triangle(xpos - w/2 - 15, ypos + h/2 - 7, xpos - w/2 - 15, ypos - h/2 + 7, xpos - w/2 - 10 - h * 1.7/2, ypos);
	}
}

class pvpButton extends Button {
    pvpButton() {
        super();
        this.ypos = height*4/6;
        this.c1 = #E87E36;
        this.c2 = #CE6721;
        this.name = "PVP";
    }
}

class howButton extends Button {
    howButton() {
        super();
        this.xpos = width/2 - 210;
        this.ypos = height*5/6;
        this.w = 380;
        this.c1 = #2FE02F;
        this.c2 = #26C426;
        this.name = "How to Play";
    }
}

class exitButton extends Button {
    exitButton() {
        super();
        this.xpos = width/2 + 210;
        this.ypos = height*5/6;
        this.w = 380;
        this.c1 = #CE2E2E;
        this.c2 = #BC2222;
        this.name = "Exit";
    }
}