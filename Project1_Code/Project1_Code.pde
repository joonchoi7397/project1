/*
	Project 1
	Name of Project: This is not an Octopus
	Author: Junyoung Choi
	Date: 2020.05.31
*/

import java.util.*;
import processing.sound.*;


Game game;
Bullet bullet = null;
loadPlayer pMaker = new loadPlayer();
SoundFile explode;
SoundFile shot;

void setup()
{
	// your code
	size(1500,800);
	noStroke();
	game = new Game();
	explode = new SoundFile(this,"explode.wav");
	shot = new SoundFile(this,"shoot.wav");
}

void draw()
{
	// your code
	background(#000269);
	
	if(game.isPlaying()) {
		if(bullet != null) {
			if(bullet.draw()) {
				bullet = null;
				game.changeTurn();
			} else {
				int hitTankId = game.checkHit(bullet);
				int damage = bullet.getPower();
				if(hitTankId != -1) {
					bullet = null;
					explode.play();
					game.getTankbyID(hitTankId).hurt(damage);
					if(!game.checkEndGame()) {
						game.changeTurn();
					}
				} else if(game.shouldGone(bullet)) {
					bullet = null;
					explode.play();
					game.changeTurn();
				}
			}
		} else {
			Tank curCan = game.getTankbyID(game.curCanon);
			if(curCan instanceof AITank) {
				if(((AITank)curCan).canUpdate()) {
					bullet = ((AITank)curCan).getbullet();
				}
			}
		}
	}
	
	rectMode(CORNER);
	game.draw();
	
	
}


// your code down here
// feel free to crate other .pde files to organize your code

void mousePressed()
{
	game.chkBtnClick();
}

void keyPressed() {
	if(game.curCanon >= 0 && game.curCanon < game.canonNum) {
		if(game.turn == 1) {
			Tank curCan = game.getTankbyID(game.curCanon);
			if(keyCode == LEFT) {
				curCan.decDist();
			} else if(keyCode == RIGHT) {
				curCan.incDist();
			} else if(keyCode == UP) {
				curCan.incAngle();
			} else if(keyCode == DOWN) {
				curCan.decAngle();
			} else if(keyCode == ENTER) {
				if(bullet == null) {
					bullet = curCan.shoot();
				}
			}
		} else if(game.gameMode == mode.PVP) {
			Tank curCan = game.getTankbyID(game.curCanon);
			if(keyCode == LEFT) {
				curCan.incDist();
			} else if(keyCode == RIGHT) {
				curCan.decDist();
			} else if(keyCode == UP) {
				curCan.incAngle();
			} else if(keyCode == DOWN) {
				curCan.decAngle();
			} else if(keyCode == ENTER) {
				if(bullet == null) {
					bullet = curCan.shoot();
				}
			}
		}	
	}
}

public enum mode {PVP, AI}
public enum state {MAIN, GAMEPLAYING, HOWTOPLAY, GAMEOVER, CHOOSINGMAP, PAUSEGAME, GAMEWIN, GAMECLEAR}

class Game extends Thread{
	Player p1;
	Player p2;
	Map gameMap;
	int level;
	mode gameMode;
	ArrayList<Button> btnlist;
	int turn;
	int canonNum;
	int curCanon;
	int winner;
	boolean gamesetting;					//doing thread or not
	state gamestate;
	private PImage htpImage;


	Game() {
		this.htpImage = loadImage("howtoplay.png");
		this.initialize();
		start();
	}

	void initialize() {
		this.gamestate = state.MAIN;
		this.gamesetting = false;
		this.btnlist = new ArrayList<Button>();
		btnlist.add(new newButton());
		btnlist.add(new setButton(0));
		btnlist.add(new pvpButton());
		btnlist.add(new howButton());
		btnlist.add(new exitButton());
		this.curCanon = -1;
	}

	void loadMapButton() {
		File f = new File(sketchPath("data/maps.json"));
		if(!f.exists()) return;

		JSONObject json = loadJSONObject("data/maps.json");

		JSONArray list = json.getJSONArray("maps");
		if(list == null) {
			return;
		}                  

		for(int i = 0; i < list.size(); i++) {
			if(i == 10) {
				break;
			}
			JSONObject tmp = list.getJSONObject(i);
			btnlist.add(new mapButton(i,tmp));
		}
	}

	void restartGame() {
		this.p1.reinit();
		this.p2.reinit();
		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
	}

	void nextLevel(int level) {
		this.level = level;
		this.p1.reinit();
		this.p2 = pMaker.makeAI(level, p1);
		this.settingCanon();

		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
	}

	void loadGame(int id, int level) {
		this.level = level;
		this.p1 = pMaker.loadfromData(id);
		this.p2 = pMaker.makeAI(level,p1);
		this.gameMode = mode.AI;
		this.settingCanon();

		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
		loadMapButton();
	}


	void newGame(boolean pvp) {
		this.level = 1;
		if(pvp) {
			this.p1 = pMaker.makeP1();
			this.p2 = pMaker.makeP2();
			this.gameMode = mode.PVP;
		} else {
			this.p1 = pMaker.makeP1();
			this.p2 = pMaker.makeAI(1, p1);
			this.gameMode = mode.AI;
		}
		this.settingCanon();
		
		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
		loadMapButton();
	}

	void gameStart() {
		this.btnlist = new ArrayList<Button>();
		this.gamestate = state.GAMEPLAYING;
		this.gamesetting = true;
	}

	void choosingMapbyJSON(JSONObject map) {
		this.gameMap = new Map(map);
	}

	void choosingMapbyLevel(int level) {
		switch (level) {
			case 15: this.gameMap = new Map(10);
				break;
			case 14:
			case 12:
			case 10: this.gameMap = new Map(6);
				break;
			case 13:
			case 7:
			case 11: this.gameMap = new Map(5);
				break;
			case 6:
			case 8: this.gameMap = new Map(4);
				break;
			case 9:
			case 5: this.gameMap = new Map(8);
				break;
			case 4: this.gameMap = new Map(2);
				break;
			case 3: 
			case 2: this.gameMap = new Map(3);
				break;
			default : this.gameMap = new Map(1);
				break;	
		}
	}

	private void settingCanon() {
		int n = 0;
		for(Tank t : p1.tankList) {
			t.setID(n++);
		}
		for(Tank t : p2.tankList) {
			t.setID(n++);
		}
		this.canonNum = n;
	}

	void draw() {
		if(isPlaying()) {
			this.gameMap.draw();
			this.p1.draw();
			this.p2.draw();
			fill(255);
			textAlign(LEFT);
			textSize(50);
			if(this.gameMode == mode.AI) {	
				text("Level : " + Integer.toString(level),0,50);
				if(mouseX < 200 && mouseX > 0 && mouseY < 100 && mouseY > 50) {
					fill(#F5F5DC);
					rectMode(CORNER);
					rect(20, 70, 250, 150, 10);
					textSize(30);
					fill(0);
					text("Gold : " + Integer.toString(p1.getMoney()), 30,110);
					text("Power : " + Integer.toString(p1.tankList.get(0).getPower()), 30, 150);
					text("MAX HP : " + Integer.toString(p1.tankList.get(0).getHP()), 30, 190);
				} else {
					text("Status",0,100);
					stroke(255);
					strokeWeight(5);
					line(156,75,173,95);
					line(173,95,190,75);
					noStroke();
				}
			} else {
				text("PVP mode",0,50);
			}

			fill(255);
			rectMode(CORNER);
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				rect(1444, 19, 12, 42);
				rect(1464, 19, 12, 42);
			} else {
				rect(1445, 20, 10, 40);
				rect(1465, 20, 10, 40);
			}
		}
		for(Button btn : btnlist) {
			btn.draw();
		}
		if(this.gamestate == state.HOWTOPLAY) {
			fill(255);
			textAlign(CENTER, CENTER);
			textSize(100);
			text("This is not an Octopus",width/2,height/6);
			drawHowToPlay();
		} else if(this.gamestate == state.GAMEOVER || this.gamestate == state.GAMEWIN) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(150);
			if(this.gameMode == mode.PVP) {
				text("Player "+ Integer.toString(winner)+" win!!", 750, 300);
			} else {
				if(this.winner == 1) {
					text("Level Clear", 750, 300);
				} else {
					text("Game Over", 750, 300);
				}
				fill(#FFD400);
				textSize(40);
				text("Gold : " + Integer.toString(p1.getMoney()), 300, 490);
				fill(255);
				text("Power : " + Integer.toString(p1.tankList.get(0).getPower()), width/2 + 165, 420);
				text("HP : " + Integer.toString(p1.tankList.get(0).getHP()), width/2 - 165, 420);	
			}
		} else if(this.gamestate == state.CHOOSINGMAP) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(50);
			text("Choosing Map",750,100);

			stroke(255);
			rectMode(CORNER);
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				strokeWeight(7);
			} else {
				strokeWeight(5);
			}
			line(1445, 20, 1485, 60);
			line(1445, 60, 1485, 20);
			noStroke();
		} else if(this.gamestate == state.GAMECLEAR) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(150);
			text("Congratulation!\nGame Clear",750,200);
			fill(#FFD400);
			textSize(40);
			text("Gold : " + Integer.toString(p1.getMoney()), 300, 490);
			fill(255);
			text("Power : " + Integer.toString(p1.tankList.get(0).getPower()), width/2 + 165, 420);
			text("HP : " + Integer.toString(p1.tankList.get(0).getHP()), width/2 - 165, 420);
		} else if(this.gamestate == state.PAUSEGAME) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(200);
			text("PAUSE",750,300);
		} else if(this.gamestate == state.MAIN) {
			fill(255);
			textAlign(CENTER, CENTER);
			textSize(100);
			text("This is not an Octopus",width/2,height/6);
		}
		
	}

	void chkBtnClick() {
		if(this.gamestate == state.HOWTOPLAY) {
			if(mouseX < 400 || mouseX > 1100) this.gamestate = state.MAIN;
			if(mouseY < 50 || mouseY > 750) this.gamestate = state.MAIN;
			if(mouseX < 1090 && mouseX > 1070 && mouseY < 80 && mouseY > 60) {
				this.gamestate = state.MAIN;
			}
			return;
		}
		if(this.gamestate == state.GAMEPLAYING) {
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				pauseGame();
			}
		}
		if(this.gamestate == state.CHOOSINGMAP) {
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				initialize();
				this.gamestate = state.MAIN;
			}
		}
		for(Button btn : btnlist) {
			if(btn.buttonClick()) {
				if(btn instanceof newButton) {
					newGame(false);
					this.initPlayer(0);
					choosingMapbyLevel(1);
					gameStart();
				} else if(btn instanceof setButton) {
					int lev = ((setButton)btn).getLevel();
					loadGame(0,lev);
					choosingMapbyLevel(lev);
					gameStart();
				} else if(btn instanceof pvpButton) {
					newGame(true);
					this.gamestate = state.CHOOSINGMAP;
				} else if(btn instanceof howButton) {
					this.gamestate = state.HOWTOPLAY;
				} else if(btn instanceof exitButton) {
					exit();
				} else if(btn instanceof restartButton) {
					restartGame();
					gameStart();
				} else if(btn instanceof nextLevelButton) {
					nextLevel(this.level + 1);
					choosingMapbyLevel(this.level+1);
					gameStart();
				} else if(btn instanceof goHomeButton) {
					initialize();
				} else if(btn instanceof mapButton) {
					mapButton mtmp = (mapButton)btn;
					choosingMapbyJSON(mtmp.getMap());
					gameStart();
				} else if(btn instanceof resumeButton) {
					resumeGame();
				} else if(btn instanceof upgradeHPButton) {
					p1.upgradeTank("HP");
					savePlayer(0);
				} else if(btn instanceof upgradePowerButton) {
					p1.upgradeTank("Power");
					savePlayer(0);
				}
			}
		}
	}

	void pauseGame() {
		this.gamestate = state.PAUSEGAME;
		btnlist.add(new goHomeButton());
		btnlist.add(new restartButton());
		btnlist.add(new resumeButton());	
	}

	void resumeGame() {
		gamestate = state.GAMEPLAYING;
		btnlist = new ArrayList<Button>();
	}

	boolean isPlaying() {
		if(this.gamestate == state.GAMEPLAYING) {
			return true;
		}
		return false;
	}

	void drawHowToPlay() {
		fill(#F5F5DC);
		rectMode(CENTER);
		rect(width/2,height/2,700,700,10);
		imageMode(CENTER);
		image(htpImage, width/2, height/2 + 10, 680, 680);
		stroke(0);
		if(mouseX < 1090 && mouseX > 1070 && mouseY < 80 && mouseY > 60) {
			strokeWeight(2);
		} else {
			strokeWeight(1);
		}
		line(1090, 80, 1070, 60);
		line(1070, 80, 1090, 60);
		noStroke();
		strokeWeight(1);
	}

	private Tank getTankbyID(int i) {
		if(p1.getTankbyID(i) == null) {
			this.turn = 2;
			return p2.getTankbyID(i);
		}
		this.turn = 1;
		return p1.getTankbyID(i);
	}

	boolean checkEndGame() {
		if(p1.checkDie()) {
			this.winner = 2;
			endGame();
			return true;
		}
		if(p2.checkDie()) {
			this.winner = 1;
			endGame();
			return true;
		}
		return false;
	}

	void initPlayer(int id) {
		File f = new File(sketchPath("data/players.json"));
		JSONObject players;
		JSONArray list;
		if(!f.exists()) {
			players = new JSONObject();
			list = new JSONArray();
		} else {
			players = loadJSONObject("data/players.json");
			list = players.getJSONArray("players");
			if(list == null) {
				list = new JSONArray();
			}
		}
		if(list.size() == 0) {
			JSONObject player = new JSONObject();
			player.setInt("id",id);
			player.setInt("level",0);
			player.setInt("power",10);
			player.setInt("money",0);
			player.setInt("maxhp",100);
			list.setJSONObject(0,player);
			players.setJSONArray("players",list);
			saveJSONObject(players,"data/players.json");
			return;
		}
		for(int i = 0; i < list.size(); i++) {
			JSONObject player = list.getJSONObject(i);
			if(player.getInt("id") == id) {
				player.setInt("level",0);
				player.setInt("power",10);
				player.setInt("money",0);
				player.setInt("maxhp",100);
				break;
			}
		}
		saveJSONObject(players,"data/players.json");
	}

	void savePlayer(int id) {
		File f = new File(sketchPath("data/players.json"));
		JSONObject players;
		JSONArray list;
		if(!f.exists()) {
			players = new JSONObject();
			list = new JSONArray();
		} else {
			players = loadJSONObject("data/players.json");
			list = players.getJSONArray("players");
			if(list == null) {
				list = new JSONArray();
			}
		}
		if(list.size() == 0) {
			JSONObject player = new JSONObject();
			player.setInt("id",id);
			player.setInt("level",this.level);
			player.setInt("power",this.p1.tankList.get(0).getPower());
			player.setInt("money",this.p1.getMoney());
			player.setInt("maxhp",this.p1.tankList.get(0).getHP());
			list.setJSONObject(0,player);
			players.setJSONArray("players",list);
			saveJSONObject(players,"data/players.json");
			return;
		}
		for(int i = 0; i < list.size(); i++) {
			JSONObject player = list.getJSONObject(i);
			if(player.getInt("id") == id) {
				if(player.getInt("level") < this.level) {
					player.setInt("level",this.level);
				}
				player.setInt("power",this.p1.tankList.get(0).getPower());
				player.setInt("money",this.p1.getMoney());
				player.setInt("maxhp",this.p1.tankList.get(0).getHP());
				break;
			}
		}
		saveJSONObject(players,"data/players.json"); 
	}


	void endGame() {
		
		if(this.gameMode == mode.AI && this.winner == 1) {
			if(this.level == 15) {
				this.gamestate = state.GAMECLEAR;
				p1.incMoney(1000);
				btnlist.add(new goHomeButton("boss"));
			} else {
				p1.incMoney(((int)(this.level / 5 + 1)) * 10);
				this.gamestate = state.GAMEWIN;
				btnlist.add(new nextLevelButton());
				btnlist.add(new goHomeButton());
			}
			savePlayer(0);
			btnlist.add(new upgradeHPButton());
			btnlist.add(new upgradePowerButton());
		} else {
			this.gamestate = state.GAMEOVER;
			if(this.gameMode == mode.AI) {
				btnlist.add(new upgradeHPButton());
				btnlist.add(new upgradePowerButton());
			}
			btnlist.add(new goHomeButton());
			btnlist.add(new restartButton());
		}
	}

	void run() {
		while(true) {
			try {
				Thread.sleep(20);
			}catch (Exception e) {}
			if(this.gamesetting) {
				int cnt = 1;
				int tmpx;
				float tmpy;
				while(cnt!=0) {
					cnt = 0;
					for(Tank tank1 : p1.tankList) {
						tmpx = tank1.getX();
						tmpy = tank1.getY();
						if(!gameMap.blockExist(tmpx,(int)(tmpy+1))) {
							tank1.setPos(tmpx,tmpy+0.025);
							cnt++;
						}
					}
					for(Tank tank2 : p2.tankList) {
						tmpx = tank2.getX();
						tmpy = tank2.getY();
						// if(!gameMap.blockExist(tmpx,(int)(tmpy + 1))) {
						// 	tank2.setPos(tmpx,tmpy+0.025);
						// 	cnt++;
						// }
						if(tank2 instanceof BossTank) {
							if(!gameMap.blockExist(tmpx,(int)(tmpy + 3))) {
								tank2.setPos(tmpx,tmpy+0.025);
								cnt++;
							}
						} else {
							if(!gameMap.blockExist(tmpx,(int)(tmpy + 1))) {
								tank2.setPos(tmpx,tmpy+0.025);
								cnt++;
							}
						}
						
					}
					try {
						Thread.sleep(20);
					}catch (Exception e) {}
				}
				for(Tank tank1 : p1.tankList) {
					tmpx = tank1.getX();
					tmpy = tank1.getY();
					tank1.setPos(tmpx,(float)((int)tmpy));
					//gameMap.setObject(tmpx,(int)tmpy);
				}
				for(Tank tank2 : p2.tankList) {
					tmpx = tank2.getX();
					tmpy = tank2.getY();
					tank2.setPos(tmpx,(float)((int)tmpy));
					//gameMap.setObject(tmpx,(int)tmpy);
				}
				this.turn = 1;
				if(gameMode == mode.AI) {
					((AIplayer)this.p2).setTarget(this.getTankbyID(0));
				}
				try {
					Thread.sleep(1000);
				}catch (Exception e) {}
				this.curCanon = 0;
				getTankbyID(0).togShooting();
				this.gamesetting = false;
			}
		}
		
	}

	void changeTurn() {
		getTankbyID(this.curCanon++).togShooting();
		if(curCanon == canonNum) {
			curCanon = 0;
		}
		Tank nTank = getTankbyID(this.curCanon);
		while(nTank.isDie()) {
			this.curCanon++;
			if(curCanon == canonNum) {
				curCanon = 0;
			}
			nTank = getTankbyID(this.curCanon);
		}

		if(nTank instanceof BossTank) {
			((BossTank)nTank).setTar(13,1000);
		} else if(nTank instanceof AITank) {
			PVector tmp = ((AIplayer)p2).computeShoot(nTank);
			((AITank)nTank).setShoot(tmp.x >= 0, tmp.y >= 500);
			((AITank)nTank).setTar(tmp.x,tmp.y);
		}
		nTank.togShooting();
	}

	boolean shouldGone(Bullet b) {
		float bx, by;
		bx = b.getX();
		by = b.getY();
		int tx, ty;
		tx = (int)bx / 75;
		ty = ((int)by - 50) / 75;
		return this.gameMap.sthExist(tx,ty);
	}

	int checkHit(Bullet b) {
		float bx, by;
		bx = b.getX();
		by = b.getY();
		int tx, ty;
		boolean tmp = false;
		for(Tank tank : p1.tankList) {
			if(!tank.isDie()) {
				tx = tank.getX() * 75 + 75;
				ty = (int)tank.getY() * 75 + 80;
				if((by - ty) * (by - ty) + (bx - tx) * (bx - tx) < 5625) {
					tmp = true;
					b.setHitId(tank.getID());
				}
			}
		}
		for(Tank tank : p2.tankList) {
			if(!tank.isDie()) {
				tx = tank.getX() * 75 + 75;
				ty = (int)tank.getY() * 75 + 80;
				if(tank instanceof BossTank) {
					if((by - ty) * (by - ty) + (bx - tx) * (bx - tx) < 15625) {
						tmp = true;
						b.setHitId(tank.getID());
					}
				} else {
					if((by - ty) * (by - ty) + (bx - tx) * (bx - tx) < 5625) {
						tmp = true;
						b.setHitId(tank.getID());
					}
				}
			}
		}
		b.setIn(tmp);
		if(b.isHit()) {
			return b.getHitId();
		}
		return -1;
	}

	

}
