class Map {
    int w;
    int h;
    boolean[][] blocks;
    boolean[][] objects;
    color c;

    Map() {
        initialize();
        for(int i = 0; i < w; i++) {
            for(int j = 0; j < h; j++) {
                objects[i][j] = false;
                if(h-j <= 2) {
                    blocks[i][j] = true;
                } else {
                    blocks[i][j] = false;
                }
            }
        }
    }

    Map(boolean[][] blocks) {
        initialize();
        for(int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                this.blocks[i][j] = blocks[i][j];
            }
        }
    }

    Map(int index) {
        initialize();
        File f = new File(sketchPath("data/maps.json"));
		if(!f.exists()) return;

		JSONObject json = loadJSONObject("data/maps.json");

		JSONArray list = json.getJSONArray("maps");
		if(list == null) {
			return;
		}

		for(int i = 0; i < list.size(); i++) {
			if(index == i+1) {
                JSONObject tmp = list.getJSONObject(i);
                JSONArray values = tmp.getJSONArray("blocks");
                if(values == null) {
                    return;
                }
                for(int k = 0; k < w; k++) {
                    for(int j = 0; j < h; j++) {
                        this.blocks[k][j] = false;
                    }
                }
                for(int k = 0; k<values.size(); k++) {
                    JSONObject tt = values.getJSONObject(k);
                    this.blocks[tt.getInt("x")][tt.getInt("y")] = true;
                }
                break;
            }
		}
    }

    Map(JSONObject json) {
        initialize();
        JSONArray values = json.getJSONArray("blocks");
        if(values == null) {
            return;
        }
        for(int i = 0; i < w; i++) {
            for(int j = 0; j < h; j++) {
                this.blocks[i][j] = false;
            }
        }
        for(int i = 0; i<values.size(); i++) {
            JSONObject tmp = values.getJSONObject(i);
            this.blocks[tmp.getInt("x")][tmp.getInt("y")] = true;
        }
    }

    void initialize() {
        this.blocks = new boolean[20][10];
        this.objects = new boolean[20][10];
        this.w = 20;
        this.h = 10;
        this.c = #3D2324;
    }

    void setBlock(int x, int y) {
        if(x < 0 || x >= w || y < 0 || y >= h) {
            return;
        }
        this.blocks[x][y] = true;
    }

    boolean blockExist(int x, int y) {
        if(x >= w || x < 0 || y < 0) return false;
        if(y >= h) return true;
        return blocks[x][y] || blocks[x+1][y];
    }

    boolean sthExist(int x, int y) {
        if(x >= w || x < 0 || y >= h || y < 0) return false;
        return blocks[x][y] || objects[x][y];
    }

    void setObject(int x, int y) {
        objects[x][y] = true;
        objects[x+1][y] = true;
        objects[x+1][y+1] = true;
        objects[x][y+1] = true;
    }

    void draw() {

        noStroke();
        fill(c);
        rectMode(CORNER);
        for(int i = 0; i < w; i++) {
            for(int j = 0; j < h; j++) {
                if(blocks[i][j]) {
                    if(j == 0) {
                        rect(75*i,75*j,75,125);
                    } else {
                        rect(75*i,75*j+50,75,75);
                    }
                }
            }
        }
    }
    
}